# Generated by Django 3.0.4 on 2020-03-23 04:50

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('social', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='link',
            options={'ordering': ['-name'], 'verbose_name': 'Enlace', 'verbose_name_plural': 'Enlaces'},
        ),
    ]
